//
//  DatabaseCoreDataMemStore.swift
//  CohesionChallenge
//
//  Created by Jason Johnson on 1/29/22.
//

import Foundation
import CoreData

class DatabaseCoreDataMemStore: DatabaseStoreProtocol {
    // This service will use core data for the user related functionality but will handle server API related functionality in memory
    // MARK: Server API Data
    static var events: [GeofenceEvent] = []
    static var locations: [GeofenceLocation] = [
        GeofenceLocation(latitude: 41.88031250000002, longitude: -87.63757181533965, identifier: "Cohesion Test Building")
    ]
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CohesionChallenge")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    var managedObjectContext: NSManagedObjectContext { return persistentContainer.viewContext }

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }


    // MARK: GeoFence API methods
    // These would be swapped out to server API calls
    func fetchGeofenceLocations(completionHandler: @escaping (() throws -> [GeofenceLocation]) -> Void) {
        completionHandler {
            return type(of: self).locations
        }
    }
    
    func fetchGeofenceEvents(completionHandler: @escaping (() throws -> [GeofenceEvent]) -> Void) {
        completionHandler {
            return type(of: self).events
        }
    }
    
    // User.inLocation will be false for exit events, true for enter events
    func sendUserEvent(userForEvent: User, completionHandler: @escaping (() throws -> Bool) -> Void) {
        let user = userForEvent
        let tEvent = GeofenceEvent(user: user.id, type: user.inLocation ? 1 : 0, timestamp: Date())
        type(of: self).events.append(tEvent)
        completionHandler {
            // We would want actual error checking here in real use but for this case, assume if it gets here it was successful
            return true
        }
    }
        
    func fetchUser(userId: UUID?, completionHandler: @escaping (() throws -> User?) -> Void) {
        let fetchRequest: NSFetchRequest<ManagedUser> = ManagedUser.fetchRequest()
        fetchRequest.fetchLimit = 1
        if let tUID = userId {
            fetchRequest.predicate = NSPredicate(format: "id == %@", tUID.uuidString)
        }
        
        do {
            var returnUser: User
            let users = try managedObjectContext.fetch(fetchRequest)
            if users.count > 0 {
                returnUser = User(id: users[0].id, inLocation: users[0].inLocation)
                completionHandler {
                    return returnUser
                }
            } else {
                completionHandler {
                    return nil
                }
            }
        } catch {
            completionHandler {
                return nil
            }
        }
    }
    
    func fetchAllUsers(completionHandler: @escaping (() throws -> [User]) -> Void) {
        //
    }
    
    func createUser(userToCreate: User, completionHandler: @escaping (() throws -> User?) -> Void) {
        let newUser = ManagedUser(context: managedObjectContext)
        
        newUser.id = userToCreate.id
        newUser.inLocation = userToCreate.inLocation
        
        do {
            try managedObjectContext.save()
            completionHandler {
                return userToCreate
            }
        } catch {
           // Add error handling here
            completionHandler {
                return nil
            }
        }
    }
    
    func updateUser(userToUpdate: User, completionHandler: @escaping (() throws -> User?) -> Void) {
        fetchUser(userId: userToUpdate.id) { (theUser: () throws -> User?) -> Void in
            do {
                if var tUser = try theUser() {
                    tUser.inLocation = userToUpdate.inLocation
                    do {
                        try self.managedObjectContext.save()
                        completionHandler {
                            return userToUpdate
                        }
                    } catch {
                       // Add error handling here
                        completionHandler {
                            return nil
                        }
                    }
                }
            } catch {
                completionHandler {
                    return nil
                }
            }
        }

    }
    
}
