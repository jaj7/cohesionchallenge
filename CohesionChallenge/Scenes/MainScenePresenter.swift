//
//  MainScenePresenter.swift
//  CohesionChallenge
//
//  Created by Jason Johnson on 1/29/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol MainScenePresentationLogic
{
    func presentGeofenceSetupResult(response: MainScene.SetupGeofence.Response)
    func presentCurrentUser(response: MainScene.GetCurrentUser.Response)
    func presentUpdatedUserStatus(response: MainScene.UpdateCurrentUserStatus.Response)
    func presentGeofenceEvents(response: MainScene.ViewGeofenceEvents.Response)
}

class MainScenePresenter: MainScenePresentationLogic
{
  weak var viewController: MainSceneDisplayLogic?
    let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter
    }()
    
    func presentGeofenceSetupResult(response: MainScene.SetupGeofence.Response)
    {
        let viewModel = MainScene.SetupGeofence.ViewModel(message: response.message)
        viewController?.displayGeofenceSetupResult(viewModel: viewModel)
    }
    
    func presentCurrentUser(response: MainScene.GetCurrentUser.Response) {
        let viewModel = MainScene.GetCurrentUser.ViewModel(user: response.user, message: response.message)
        viewController?.displayCurrentUserInfo(viewModel: viewModel)
    }
    
    func presentUpdatedUserStatus(response: MainScene.UpdateCurrentUserStatus.Response) {
        let viewModel = MainScene.UpdateCurrentUserStatus.ViewModel(user: response.user, message: response.message)
        viewController?.displayUpdatedUserStatus(viewModel: viewModel)
    }
    
    func geofenceEventTypeToString(type: Int) -> String {
        switch (type) {
            case EXITED_LOCATION: return "EXIT"
            case ENTERED_LOCATION: return "ENTER"
            default: return "UNKNOWN"
        }
    }
    
    func presentGeofenceEvents(response: MainScene.ViewGeofenceEvents.Response) {
        var tEvents : [MainScene.ViewGeofenceEvents.FormattedEvent] = []
        for event in response.events {
            let date = dateFormatter.string(from: event.timestamp)
            let tNewEvent = MainScene.ViewGeofenceEvents.FormattedEvent(userId: event.user.uuidString, type: geofenceEventTypeToString(type: event.type), timestamp: date)
            tEvents.append(tNewEvent)
        }
        let viewModel = MainScene.ViewGeofenceEvents.ViewModel(events: tEvents)
        viewController?.displayGeofenceEvents(viewModel: viewModel)
    }
}
