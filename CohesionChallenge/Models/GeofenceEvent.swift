//
//  GeofenceEvent.swift
//  CohesionChallenge
//
//  Created by Jason Johnson on 1/29/22.
//

import Foundation

struct GeofenceEvent : Equatable {
    var user : UUID
    var type : Int  // 1 - Entry, 0 - Exit
    var timestamp: Date
}

func ==(lhs: GeofenceEvent, rhs: GeofenceEvent) -> Bool {
    return lhs.user == rhs.user
        && lhs.type == rhs.type
        && lhs.timestamp == rhs.timestamp
}
