//
//  ManagedUser+CoreDataProperties.swift
//  CohesionChallenge
//
//  Created by Jason Johnson on 1/29/22.
//
//

import Foundation
import CoreData


extension ManagedUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedUser> {
        return NSFetchRequest<ManagedUser>(entityName: "ManagedUser")
    }

    @NSManaged public var id: UUID
    @NSManaged public var inLocation: Bool

}

extension ManagedUser : Identifiable {

}
