//
//  User.swift
//  CohesionChallenge
//
//  Created by Jason Johnson on 1/29/22.
//

import Foundation

struct User : Equatable {
    var id : UUID
    var inLocation: Bool
}

func ==(lhs: User, rhs: User) -> Bool {
    return lhs.id == rhs.id
        && lhs.inLocation == rhs.inLocation
}
