//
//  ManagedUser+CoreDataClass.swift
//  CohesionChallenge
//
//  Created by Jason Johnson on 1/29/22.
//
//

import Foundation
import CoreData

//@objc(ManagedUser)
public class ManagedUser: NSManagedObject {
    public override var description: String {
           return "ManagedUser"
       }
}
