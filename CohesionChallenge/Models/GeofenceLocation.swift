//
//  GeofenceLocation.swift
//  CohesionChallenge
//
//  Created by Jason Johnson on 1/29/22.
//

import Foundation

struct GeofenceLocation : Equatable {
    var latitude : Double
    var longitude : Double
    var identifier: String
}

func ==(lhs: GeofenceLocation, rhs: GeofenceLocation) -> Bool {
    return lhs.latitude == rhs.latitude
        && lhs.longitude == rhs.longitude
        && lhs.identifier == rhs.identifier
}
