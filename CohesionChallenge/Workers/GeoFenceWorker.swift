//
//  GeoFenceWorker.swift
//  CohesionChallenge
//
//  Created by Jason Johnson on 1/29/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import CoreLocation

let EXITED_LOCATION = 0
let ENTERED_LOCATION = 1

class GeoFenceWorker : NSObject, CLLocationManagerDelegate
{
    let locationManager = CLLocationManager()
    static let dbWorker = DatabaseWorker(dbStore: DatabaseCoreDataMemStore())
    static var observers = [LocationObserver]()
    var currentUser: User?
    
    init(locations: [GeofenceLocation]) {
        super.init()
        
        getCurrentUser()
        
        if locationManager.authorizationStatus != .authorizedAlways {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.delegate = self
        
        // Load locations into the manager in order to monitor them.
        for location in locations {
            let geofenceRegionCenter =  CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: 100, identifier: location.identifier)
            geofenceRegion.notifyOnExit = true
            geofenceRegion.notifyOnEntry = true
            locationManager.startMonitoring(for: geofenceRegion)
        }

        
    }
    
    private func getCurrentUser() {
        let defaults = UserDefaults.standard
        if let tUserID = defaults.value(forKey: "User") as? String {
            let tUID = UUID(uuidString: tUserID)
            type(of:self).dbWorker.fetchUser(userId: tUID) { theUser in
                if let retrievedUser = theUser {
                    self.currentUser = retrievedUser
                } else {
                    print("Could not retrieve user with ID: \(tUserID)")
                }
            }
        } else {
            print("No user ID available to fetch.")
        }
    }
    
    private func sendUserGeofenceEvent(eventCode: Int) {
        if var tUser = self.currentUser {
            tUser.inLocation = (eventCode == ENTERED_LOCATION)
            type(of: self).dbWorker.sendUserEvent(userForEvent: tUser) { success in
                // TODO:  Need to add handling of failures here but for now assume success
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("Region \(region.identifier) was entered")
        sendUserGeofenceEvent(eventCode: ENTERED_LOCATION)
        notifyObservers(status: ENTERED_LOCATION)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("Region \(region.identifier) was exited")
        sendUserGeofenceEvent(eventCode: EXITED_LOCATION)
        notifyObservers(status: EXITED_LOCATION)
    }
    
    // MARK:  Observer management
    func registerObserver(observer: LocationObserver) {
        type(of: self).observers.append(observer)
    }
    
    func notifyObservers(status: Int) {
        for observer in type(of: self).observers {
            observer.locationUpdate(status: status)
        }
    }
    
    func deregisterObserver(observer: LocationObserver) {
        if let removed = type(of: self).observers.enumerated().first(where: { $0.element.locationObserverID == observer.locationObserverID })
        {
            type(of: self).observers.remove(at: removed.offset)
        }
    }
}

protocol LocationObserver {
    var locationObserverID : Int { get }
    func locationUpdate(status : Int)
}
