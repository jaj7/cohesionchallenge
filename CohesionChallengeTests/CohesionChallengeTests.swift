//
//  CohesionChallengeTests.swift
//  CohesionChallengeTests
//
//  Created by Jason Johnson on 1/29/22.
//

import XCTest
@testable import CohesionChallenge

class CohesionChallengeTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // CoreDataMemoryStore Tests
    // Check that record count coming back is right
    func testGeofenceLocationRetrieval() throws {
        let dataStore = DatabaseCoreDataMemStore()
        
        dataStore.fetchGeofenceLocations { locations in
            do {
                let tLocations = try locations()
                XCTAssertTrue(tLocations.count == 1)
            } catch {
                XCTFail()
            }

        }
    }

    // Ensure that the record being received has the correct data
    func testGeofenceLocationRetrievalDataCheck() throws {
        let dataStore = DatabaseCoreDataMemStore()
        
        dataStore.fetchGeofenceLocations { locations in
            do {
                let tLocations = try locations()
                XCTAssertTrue(tLocations[0].latitude == 41.88031250000002)
                XCTAssertTrue(tLocations[0].longitude == -87.63757181533965)
                XCTAssertTrue(tLocations[0].identifier == "Cohesion Test Building")
            } catch {
                XCTFail()
            }

        }
    }

    func testGeofenceEventSent() throws {
        let dataStore = DatabaseCoreDataMemStore()

        let testUID = UUID()
        let testUser = User(id: testUID, inLocation: true)

        dataStore.sendUserEvent(userForEvent: testUser) { success in
            do {
                let tSuccess = try success()
                XCTAssertTrue(tSuccess)
                dataStore.fetchGeofenceEvents { events in
                    do {
                        let tEvents = try events()
                        XCTAssertTrue(tEvents[0].user == testUID)
                        XCTAssertTrue(tEvents[0].type == ENTERED_LOCATION)
                    } catch {
                        XCTFail()
                    }
                }
            } catch {
                XCTFail()
            }
        }
    }
    
//    func testSendEnterRecord() throws {
//        var dbWorker = databaseWorker(dbStore: )
//    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
